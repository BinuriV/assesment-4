#include <stdio.h>
int main()
{

  int number, rev=0, remainder;
  printf("Enter an integer: ");
  scanf("%d", &number);
  
  while (number != 0) {
    remainder = number % 10;
    rev = rev * 10 + remainder;
    number /= 10;
   }
   
   printf("Reversed number = %d \n", rev);
   return(0);
   
 }
