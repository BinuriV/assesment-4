#include <stdio.h>
int main() 
{
    int number, m;
    printf("Enter a positive integer: \n");
    scanf("%d", &number);
    printf("Factors of %d are: \n", number);
    
    for (m = 1; m <= number; ++m) 
    {
        if (number % m == 0) 
        {
            printf("%d \n", m);
        }
    }
    return 0;
    
}   
